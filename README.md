# hola
Author: Eric Fu
## Description
This repository contains pipeline codes used by all exchange services. 

## Pipeline Files

The `/.gitlab-common.yml` and the `/.gitlab-ui.yml` in this repository is imported in the `/.gitlab-ci.yml` file of other repositories.

When you update the codes of this repository, it may not immediately take effects if you retry the pipeline of other repositories. This is because cache is enabled so that they may not using newest pipeline code. To fix it, you may have to click the green **Run Pipeline** button in the CI/CD --> Pipelines Page, for example, [Pipeline - ui.exchange ](https://gitlab.com/SDCE/exchange-ui/ui.exchange/pipelines).

## Dockerfile

`/build.sh` uses the `/Dockerfile` and `/Dockerfile-python` to build docker images and push to Alicloud Container Registry.

- Dockerfile
    - used to build golang-based docker images
- Dockerfile-python
    - used to build id3global python-based docker images

## Environment Variables
- *GIT_USER*, *GIT_ASKPASS*, *ALI_REPOSITORY_USERNAME*, *ALI_REPOSITORY_PASSWORD* are stored in the gitlab environment variables
- *REPOSITORY_NAME* is passed from corresponding pipeline
- *BUILD_URL* is passed from corresponding pipeline