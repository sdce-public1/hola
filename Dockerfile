# build stage
FROM golang:1.19-alpine AS build_base

RUN apk add bash ca-certificates git gcc g++ libc-dev linux-headers
WORKDIR /go/src/gitlab.com/sdce/service/

# Force the go compiler to use modules
ARG GIT_USER=test
ARG GIT_ASKPASS=test

RUN git config --global credential.helper store && \
    echo "https://${GIT_USER}:${GIT_ASKPASS}@gitlab.com" > ~/.git-credentials

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .
#This is the ‘magic’ step that will download all the dependencies that are specified in 
# the go.mod and go.sum file.
# Because of how the layer caching system works in Docker, the  go mod download 
# command will _ only_ be re-run when the go.mod or go.sum file change 
# (or when we add another docker instruction this line)
RUN go mod download

# This image builds the weavaite server
FROM build_base AS server_builder
# Here we copy the rest of the source code
COPY . .
# And compile the project
ADD . /src
# Default build url
ARG BUILD_URL=cmd/service/*.go
RUN echo ${BUILD_URL} && \
    CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o entrypoint /src/${BUILD_URL}

RUN GRPC_HEALTH_PROBE_VERSION=v0.3.0 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# final stage
FROM alpine
WORKDIR /app
RUN apk add tzdata ca-certificates
# COPY --from=server_builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=server_builder /bin/grpc_health_probe /bin/grpc_health_probe
COPY --from=server_builder /go/src/gitlab.com/sdce/service/entrypoint /app/
ENTRYPOINT ./entrypoint
